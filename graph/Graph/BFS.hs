{-# LANGUAGE ScopedTypeVariables #-}

-- |
module Graph.BFS where

import qualified Graph.G as G
import Graph.Tree
import qualified Graph.Tree as T
import Optics

bfs :: forall v. [Tree v] -> [v]
bfs = ($ []) . foldrOf folded f b
  where
    f :: Tree v -> ([[Tree v]] -> [v]) -> [[Tree v]] -> [v]
    f (N (v, cs)) fs = \tss -> v : fs (cs : tss)

    b :: [[Tree v]] -> [v]
    b [] = []
    b tss = foldlOf' folded (foldrOf folded f) b tss []

-- f' :: ([[Tree v]] -> [v]) -> [Tree v] -> [[Tree v]] -> [v]
-- f' = foldrOf folded f

-- >>> bfs $ (view G.graph $ G.mk ('a','j') [('a','b'), ('a','f'), ('b','c'), ('b','e'),('c','a'),('c','d'),('e','d'),('g','h'),('g','j'),('h','f'),('h','i'),('h','j')]) `T.tree` ['a' .. 'j']
-- "agbfhceijd"

-- | Breadth First Enumaration
bfe :: forall v. Tree v -> [v]
bfe = ($ []) . (`go` b)
  where
    go (N (v, cs)) fs = \ts -> v : fs (cs : ts)
    b [] = []
    b ts = foldl (foldr go) b ts []

-- >>> fmap bfe $ (view G.graph $ G.mk ('a','j') [('a','b'), ('a','f'), ('b','c'), ('b','e'),('c','a'),('c','d'),('e','d'),('g','h'),('g','j'),('h','f'),('h','i'),('h','j')]) `T.tree` ['c', 'g']
-- ["cabfed","ghij"]
