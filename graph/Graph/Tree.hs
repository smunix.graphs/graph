{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ViewPatterns #-}

module Graph.Tree where

import Data.Array
import Data.Function (fix)
import Graph.M (M)
import qualified Graph.M as M
import Optics

---------------
-- Rose tree --
---------------
newtype Tree v = N (v, [Tree v])
  deriving (Show)

tree :: forall v. Ix v => Array v [v] -> [v] -> [Tree v]
tree arr@(bounds -> bnds) = prune . (<&> fix \rec v -> N (v, arr ! v <&> rec))
  where
    prune :: [Tree v] -> [Tree v]
    prune = M.run bnds . prune'
      where
        prune' :: [Tree v] -> M v [Tree v]
        prune' [] = pure []
        prune' (N (v, cs) : ts) = M.on v (prune' ts) do
          M.set v
          (:) <$> (N . (v,) <$> prune' cs) <*> prune' ts
