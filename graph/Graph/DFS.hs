{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE ViewPatterns #-}

module Graph.DFS where

import Control.Monad.ST
import Data.Array
import Data.Array.ST
import Data.Function (fix)
import Graph.G (G, graph)
import qualified Graph.G as G
import Graph.M (M)
import qualified Graph.M as M
import Graph.Tree
import Optics hiding (indices)

-------------
-- Helpers --
-------------

topoH :: forall v. Ix v => Array v [v] -> [v]
topoH arr@(indices -> vs) = tree arr vs & fix (\rec -> foldMap \(N (v, cs)) -> rec cs :> v) & reverse

dfsH :: forall v. Ix v => Array v [v] -> [v]
dfsH arr@(indices -> vs) = tree arr vs & fix \rec -> foldMap \(N (v, cs)) -> v :< rec cs

-----------------------------------------------
-- Algorithm 1. Depth-first search numbering --
-----------------------------------------------

dfsSort :: forall v. Ix v => G v -> Array v Int
dfsSort g@(view graph -> garr@(bounds -> bnds)) = go $ dfsH garr
  where
    go :: [v] -> Array v Int
    go = array bnds . (`zip` [1 ..])

-- >>> dfsSort $ G.mk ('a','j') [('a','b'), ('a','f'), ('b','c'), ('b','e'),('c','a'),('c','d'),('e','d'),('g','h'),('g','j'),('h','f'),('h','i'),('h','j')]
-- array ('a','j') [('a',1),('b',2),('c',3),('d',4),('e',5),('f',6),('g',7),('h',8),('i',9),('j',10)]

--------------------------------------
-- Algorithm 2. Topological sorting --
--------------------------------------

topSort :: forall v. Ix v => G v -> Array v Int
topSort g@(view graph -> garr@(bounds -> bnds)) = go $ topoH garr
  where
    go :: [v] -> Array v Int
    go = array bnds . (`zip` [1 ..])

-- >>> topSort $ G.mk ('a','j') [('a','b'), ('a','f'), ('b','c'), ('b','e'),('c','a'),('c','d'),('e','d'),('g','h'),('g','j'),('h','f'),('h','i'),('h','j')]
-- array ('a','j') [('a',5),('b',7),('c',9),('d',10),('e',8),('f',6),('g',1),('h',2),('i',4),('j',3)]

---------------------------------------
-- Algorithm 3. Connected components --
---------------------------------------

cc :: forall v. Ix v => G v -> [Tree v]
cc g@(view graph -> garr@(indices -> vs)) = tree garr vs

-- >>> G.mk ('a','j') [('a','b'), ('a','f'), ('b','c'), ('b','e'),('c','a'),('c','d'),('e','d'),('g','h'),('g','j'),('h','f'),('h','i'),('h','j')]
-- G {_graph = array ('a','j') [('a',"bf"),('b',"ce"),('c',"ad"),('d',""),('e',"d"),('f',""),('g',"hj"),('h',"fij"),('i',""),('j',"")]}

-- >>> cc $ G.mk ('a','j') [('a','b'), ('a','f'), ('b','c'), ('b','e'),('c','a'),('c','d'),('e','d'),('g','h'),('g','j'),('h','f'),('h','i'),('h','j')]
-- [N ('a',[N ('b',[N ('c',[N ('d',[])]),N ('e',[])]),N ('f',[])]),N ('g',[N ('h',[N ('i',[]),N ('j',[])])])]

------------------------------------------------
-- Algorithm 4. Strongly connected components --
------------------------------------------------

scc' :: forall v. Ix v => G v -> [Tree v]
scc' g@(G.transpose -> (view graph -> garr')) = tree garr' (topoH garr)
  where
    garr :: Array v [v]
    (view graph -> garr) = g

-- >>> scc' $ G.mk ('a','j') [('a','b'), ('a','f'), ('b','c'), ('b','e'),('c','a'),('c','d'),('e','d'),('g','h'),('g','j'),('h','f'),('h','i'),('h','j')]
-- [N ('g',[N ('j',[]),N ('h',[N ('i',[]),N ('f',[])])]),N ('a',[N ('b',[N ('e',[N ('d',[])]),N ('c',[])])])]

scc :: forall v. Ix v => G v -> [Tree v]
scc g@(G.transpose -> (view graph -> garr')) = tree garr (topoH garr')
  where
    garr :: Array v [v]
    (view graph -> garr) = g

-- >>> scc $ G.mk ('a','j') [('a','b'), ('a','f'), ('b','c'), ('b','e'),('c','a'),('c','d'),('e','d'),('g','h'),('g','j'),('h','f'),('h','i'),('h','j')]
-- [N ('g',[N ('h',[N ('f',[]),N ('i',[]),N ('j',[])])]),N ('a',[N ('b',[N ('c',[N ('d',[])]),N ('e',[])])])]

------------------------------------
-- Algorithm 5. Classifying edges --
------------------------------------

back :: forall v. Ix v => G v -> G v
back g@(view graph -> garr@(assocs -> es)) = G.G $ array (bounds garr) (select es)
  where
    topOrder :: Array v Int
    topOrder = topSort g

    select :: [(v, [v])] -> [(v, [v])]
    select (fmap (\(v, (^.. (folded % filtered \w -> topOrder ! w < topOrder ! v)) -> ws') -> (v, ws')) -> oes) = oes

-- >>> back $ G.mk ('a','j') [('a','b'), ('a','f'), ('b','c'), ('b','e'),('c','a'),('c','d'),('e','d'),('g','h'),('g','j'),('h','f'),('h','i'),('h','j')]
-- G {_graph = array ('a','j') [('a',""),('b',""),('c',"a"),('d',""),('e',""),('f',""),('g',""),('h',""),('i',""),('j',"")]}

cross :: forall v. Ix v => G v -> G v
cross g@(view graph -> garr@(assocs -> es)) = G.G $ array (bounds garr) (select es)
  where
    topOrder :: Array v Int
    topOrder = topSort g

    dfsOrder :: Array v Int
    dfsOrder = dfsSort g

    select :: [(v, [v])] -> [(v, [v])]
    select (fmap (\(v, (^.. (folded % filtered \w -> topOrder ! v < topOrder ! w && dfsOrder ! w < dfsOrder ! v)) -> ws') -> (v, ws')) -> oes) = oes

-- >>> cross $ G.mk ('a','j') [('a','b'), ('a','f'), ('b','c'), ('b','e'),('c','a'),('c','d'),('e','d'),('g','h'),('g','j'),('h','f'),('h','i'),('h','j')]
-- G {_graph = array ('a','j') [('a',""),('b',""),('c',""),('d',""),('e',"d"),('f',""),('g',""),('h',"f"),('i',""),('j',"")]}

-------------------------------------
-- Algorithm 6. Reachable vertices --
-------------------------------------

reachable :: forall v. Ix v => G v -> [v] -> [v]
reachable (view graph -> garr) = M.run (bounds garr) . foldMap go . tree garr
  where
    go :: Tree v -> M v [v]
    go (N (v, cs)) = M.on v mempty do
      M.set v
      cs & foldMap go <&> (v :)

-- >>> reachable (G.mk ('a','j') [('a','b'), ('a','f'), ('b','c'), ('b','e'),('c','a'),('c','d'),('e','d'),('g','h'),('g','j'),('h','f'),('h','i'),('h','j')]) "c"
-- "cabedf"

-----------------------------------------
-- Algorithm 7. Biconnected components --
-----------------------------------------
