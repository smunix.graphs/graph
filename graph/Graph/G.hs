{-# LANGUAGE ViewPatterns #-}

-- |
module Graph.G where

import Data.Array
import Optics hiding (indices)

newtype G v = G {_graph :: Array v [v]}
  deriving (Show)

makeLenses ''G

mk :: Ix v => (v, v) -> [(v, v)] -> G v
mk b = G . accumArray (:>) [] b

-- >>> mk ('a','j') [('a','b'), ('a','f'), ('b','c'), ('b','e'),('c','a'),('c','d'),('e','d'),('g','h'),('g','j'),('h','f'),('h','i'),('h','j')]
-- G {_graph = array ('a','j') [('a',"bf"),('b',"ce"),('c',"ad"),('d',""),('e',"d"),('f',""),('g',"hj"),('h',"fij"),('i',""),('j',"")]}

vertices :: Ix v => G v -> [v]
vertices = view (graph % to indices)

edges :: Ix v => G v -> [(v, v)]
edges g@(view graph -> garr) = do v <- indices garr; v' <- garr ! v; pure (v, v')

outdeg :: (Ix v) => G v -> Array v Int
outdeg g@(view graph -> garr) = array (bounds garr) $ indices garr <&> \v -> (v, garr ! v & length)

indeg :: (Ix v) => G v -> Array v Int
indeg = outdeg . transpose

rev :: Ix v => G v -> [(v, v)]
rev = reverse . edges

transpose :: Ix v => G v -> G v
transpose g = mk (g ^. (graph % to bounds)) (rev g)
