{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE RankNTypes #-}

module Graph.M where

import Control.Monad
import Control.Monad.ST
import Data.Array.ST
import Optics

---------------------
-- Monadic actions --
---------------------

newtype M n a = M {_unM :: forall s. STUArray s n Bool -> ST s a}
  deriving (Functor)

makeLenses ''M

instance Applicative (M n) where
  pure a = M \_ -> pure a
  (<*>) = ap

instance Monad (M n) where
  M m >>= f = M \s -> do a <- m s; (f a ^. unM) s

run :: forall n a. Ix n => (n, n) -> M n a -> a
run bnds m = runST do bnds `newArray` False >>= (m ^. unM)

set :: forall n. (Ix n) => n -> M n ()
set n = M $ flip (`writeArray` n) True

on :: forall n a. Ix n => n -> M n a -> M n a -> M n a
on n t f = M \s -> do readArray s n >>= \case { False -> f ^. unM $ s; True -> t ^. unM $ s }

instance (Semigroup a) => Semigroup (M n a) where
  a <> b = do a' <- a; b' <- b; pure $ a' <> b'

instance (Monoid a) => Monoid (M n a) where
  mempty = pure mempty
