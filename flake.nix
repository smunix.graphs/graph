{
  description =
    "Lazy Depth First search and Linear Graph Algorithms in Haskell";
  inputs = {
    np.url = "github:nixos/nixpkgs?ref=master";
    fu.url = "github:numtide/flake-utils?ref=master";
    hls.url = "github:haskell/haskell-language-server?ref=master";
    nf.url = "github:numtide/nix-filter?ref=master";
    ds.url = "github:numtide/devshell?ref=master";
    emacs.url = "github:nix-community/emacs-overlay?ref=master";
  };
  outputs = { self, np, fu, hls, nf, ds, emacs }:
    with np.lib;
    with fu.lib;
    eachSystem [ "x86_64-linux" ] (system:
      let
        version =
          "${substring 0 8 self.lastModifiedDate}.${self.shortRev or "dirty"}";
        config = { };
        overlay = final: _:
          with self;
          with haskell.lib;
          with final.haskellPackages.extend (_: _: { }); {
            graph = (callCabal2nix "graph" ./. { }).overrideAttrs (o: {
              version = "${o.version}.${version}";
              doCheck = true;
            });
          };
        overlays = [ overlay hls.overlay ds.overlay emacs.overlay ];
      in with (import np { inherit config system overlays; }); rec {
        packages = flattenTree (recurseIntoAttrs { inherit graph; });
        defaultPackage = packages.graph;
        inherit overlays;
        devShell = devshell.mkShell {
          name = "GRAPH";
          packages =
            [ cabal-install emacsGcc haskell-language-server hpack ghc ];
          commands = [
            {
              name = "save-all";
              category = "git";
              help = "save all files in git";
              command = ''
                #!/usr/bin/env bash
                pushd $PRJ_ROOT
                git status
                hpack -f
                ${haskellPackages.implicit-hie}/bin/gen-hie --cabal > hie.yaml
                git add --all
                git status
                popd
              '';
            }
            {
              name = "watch-files";
              category = "build";
              help = "watch builds";
              command = ''
                #!/usr/bin/env bash
                ${ghcid}/bin/ghcid -W -a -c 'cabal repl lib:graph'
              '';
            }
          ];
        };
        # with haskellPackages;
        # shellFor {
        #   packages = _: [ ];
        #   buildInputs = [ cabal-install haskell-language-server ghc ];
        # };
      });
}
